import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    //定义一组状态 规定每个状态下 的属性
    Item{
        id:root
        state:"red"
        states: [
            State {
                //红灯状态 红灯亮 其它灭
                name: "red"
                PropertyChanges {target: red;color:"red"}
                PropertyChanges {target: yellow;color:"gray"}
                PropertyChanges {target: green;color:"gray"}
            },
            State {
                name: "yellow"
                PropertyChanges {target: yellow;color:"yellow"}
                PropertyChanges {target: red;color:"gray"}
                PropertyChanges {target: green;color:"gray"}
            },
            State {
                name: "green"
                PropertyChanges {target: green;color:"green"}
                PropertyChanges {target: red;color:"gray"}
                PropertyChanges {target: yellow;color:"gray"}
            }
        ]

        //过渡
        transitions: [
            Transition {
                //状态 红灯 到 黄灯的过渡
                from: "red"; to: "yellow"
                ColorAnimation { target: red; properties: "color"; duration: 500 }
                ColorAnimation { target: yellow; properties: "color"; duration: 500 }
            },
            Transition {
                from: "yellow"; to: "green"
                ColorAnimation { target: yellow; properties: "color"; duration: 500 }
                ColorAnimation { target: green; properties: "color"; duration: 500 }
            }
        ]
    }

    //三个灯的按钮
    Row{
        anchors.centerIn: parent
        spacing: 20
        //红灯
        Rectangle{
            id:red
            width: 50
            height: 50
            radius: 50
            color: "red"
        }

        //黄灯
        Rectangle{
            id:yellow
            width: 50
            height: 50
            radius: 50
            color: "yellow"
        }

        //绿灯
        Rectangle{
            id:green
            width: 50
            height: 50
            radius: 50
            color: "green"
        }
    }

    //依次切换三个状态
    Button{
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        text: qsTr("切换")
        onClicked: {
            if(root.state=="red")
                root.state="yellow"
            else if(root.state=="yellow")
                root.state="green"
            else
                root.state="red"
        }
    }
}
